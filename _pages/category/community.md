---
title: "Community"
layout: community
category: community
permalink: "/community"
---
At SocialHub we are building the Fediverse together. Our members all dedicate their time to the benefit of all fedizens. Cooperation for us is very important. In fact, it is vital. Without open standards and a healthy technology landscape, interoperability between our apps would be impossible. We strive to take Fediverse to the next level, far into the future. [**_"Reimagining Social Networking"_**](/2021/04/social-networking-reimagined/) is what we do.

We invite you to join us in our quest, to discover our work, and help create humane technology. It does not matter what skills you have, whether you want to develop or help spread our message. We need everyone, so hesitate no longer. Get involved!