---
title: "Support the SocialHub Community"
layout: page-sidebar
permalink: "/support.html"
---

SocialHub is the grassroots community of technologists and fedizens that dedicate themselves to the success of the Fediverse. Together we are [**"United in Diversity"**](/about/) and volunteer time and effort on a common quest to envision [**"Social Networking Reimagined"**](/2021/04/social-networking-reimagined/).

The Fediverse is still but fragile, its future by no means secure. What we build is analogous to [**"Spiral Island"**](/2021/04/fediverse-spiral-island-analogy/), a man-made structure floating in rough seas. We at SocialHub extend its base: The Fediverse technology foundation, upon which anyone can build great applications and services.

We want to shape beautiful archipelago's that delight people with what they provide. And we really need your help to enable us to do so. Here's what you can do right now:

- Sign up to the SocialHub forum and get to know fellow members.
- Don't be shy to participate in discussion and start new topics.
- You don't need to be tech-savvy. We welcome any passionate fedizen.
- Good starting places are [Fediverse Futures](https://socialhub.activitypub.rocks/c/fediversity/fediverse-futures/58) and [Community](https://socialhub.activitypub.rocks/c/community/2) categories.

Sooo... in other words:

<a class="btn btn-danger" href="https://socialhub.activitypub.rocks/c/uncategorized/1">We'd Love to Welcome You &nbsp;<i class="fa fa-spinner fa-spin fa-fw" aria-hidden="true"></i></a>

---

Currently we do not receive any financial support for our contributions. But given the importance of community work, this may change in the future. Join the discussion on [Organizing for Community Empowerment](https://socialhub.activitypub.rocks/t/organizing-for-socialhub-community-empowerment/1529) to give your opinion and feedback.

Thank you so much!
