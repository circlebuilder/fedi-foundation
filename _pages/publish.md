---
title: "Publish to Fedi Foundation"
layout: page-sidebar
permalink: "/publish.html"
---

Publishing to the Fedi Foundation website is relatively easy. This tutorial will take you through the process step by step.

#### Contents

- [Prerequisites](#prerequisites)
- [Publishing pages](#publishing-pages)
  - [Site model](#site-model)
  - [Page design](#page-design)
  - [Folder structure](#folder-structure)
- [Licensing](#licensing)
- [Adding authors](#adding-authors)
  - [Provide author details](#provide-author-details)
  - [Create member profile](#create-member-profile)
- [Editing content](#editing-content)
  - [Using the browser](#using-the-browser)
  - [Using Git](#using-git)

---

## Prerequisites

This website is created with [Jekyll](https://jekyllrb.com/) and both source and content is hosted on [Codeberg](https://codeberg.org/). In order to edit the site content you need the following:

- A user account at [Codeberg](https://codeberg.org).
- Membership at [SocialHub](https://socialhub.activitypub.rocks) to be attributed as author.
- Basic understanding of [Markdown](https://www.markdownguide.org/basic-syntax/) syntax.
- Know how to [Edit content](#edit-content) and submit changes for review.
- Have [Git](https://git-scm.com/) installed when not using the browser user interface.


## Publishing pages

#### Site model

The information model of the website works according to the following rules:

- There are **Pages** and **Posts**.
- Posts are shown on the **Landing Page**.
  - The most **Recent Post** is shown first and is more prominent.
  - Next three posts appear to the left of most recent post.
  - A single **Sticky Post** is displayed in a yellow card design.
  - All **Other Posts** appear in a paginated view below.
  - **Featured Posts** appear in a paginated list on the left beside other Posts.
- A Post **Category** determines the site section to which it belongs.
  - Categories are shown as entries in the **Top Menu**.
- A Post may have one or more **Tags**.
  - The **First Tag** is used to categorize the Post in section pages.
  - First Tags are category-specific (see the list below).
  - Undefined First Tags for a Category are categorized as **Other**. 
- Pages are not automatically added to the site hierarchy.
  - A Page must be specifically linked from other content.

These are the First Tags that are defined for each Category:

- [**Learn**](/learn.html): <span class="taglist"><a href="/tags.html#tutorials">tutorials</a>, <a href="/tags.html#reference">reference</a>, <a href="/tags.html#guide">guide</a>, <a href="/tags.html#best-practice">best-practice</a></span>
- [**Ideate**](/ideate.html): <span class="taglist"><a href="/tags.html#vision">vision</a>, <a href="/tags.html#design">design</a>, <a href="/tags.html#pattern">pattern</a>, <a href="/tags.html#brainstorm">brainstorm</a></span>
- [**Create**](/create.html): <span class="taglist"><a href="/tags.html#research">research</a>, <a href="/tags.html#specification">specification</a>, <a href="/tags.html#fep">fep</a>, <a href="/tags.html#code">code</a></span>
- [**Foster**](/foster.html): <span class="taglist"><a href="/tags.html#advocacy">advocacy</a>, <a href="/tags.html#process">process</a>, <a href="/tags.html#activity">activity</a>, <a href="/tags.html#support">support</a></span>
- [**Community**](/community.html): _does not support First Tag sub-categories_{: .text-muted }

So other tags are still allowed, but just aren't used as categories on section pages.

#### Page design

A page consists of two parts: it starts with **front matter** followed by **markdown content**. The front matter determines how the page is displayed. Below is the front matter for [this page](/2021/03/introducing-fedi-foundation/): 

```markdown
---
title: "Introducing the Federated Diversity Foundation"
description: "Foundation for community empowerment, encouraging healthy evolution of Fediverse technology."
author: aschrijver
license: CC-BY-NC-SA
categories: community
tags: [vision, roadmap]
status: sticky
image: pexels-anna-shvets-4312861.jpg
---
```

The following table explains all the supported front matter fields.

| Variable | Description |
| :---: | :--- |
| `title` | _Required_. The title of the page, that is displayed as a Level 1 header, and is also added to the browser tab title. |
| `description` | _Required_. The description shows on the left side-bar in Posts, and also in link previews ([OpenGraph](https://ogp.me/), [Twitter Cards](https://developer.twitter.com/en/docs/twitter-for-websites/cards/guides/getting-started)). Keep the description short and snappy. |
| `author` | _Required for Posts, optional for Pages_. The value you supply to this field must refer to an entry in `people.yml` and must correspond to a valid SocialHub member account name. An array of multiple authors is supported. See [Adding authors](#adding-authors) for more information. |
| `license` | _Optional, defaults to [CC0 Public Domain](http://creativecommons.org/publicdomain/zero/1.0/)_. As author you can choose to override the public domain license, and opt instead for Creative Commons Share-Alike by specifying `CC-BY-SA` or choose Creative Commons Non-Commercial Share-Alike with `CC-BY-NC-SA`. See [licensing details](#licensing) for more info. |
| `categories` | _Required for Posts_. Though the field name is plural, only a single value is supported. See [Site model](#site-model) for available Categories to choose from. |
| `tags` | _Optional, only applies to Posts_. One or more tags can be supplied in an array. Use of tags is recommended. If there are no tags, or the First Tag is unknown for the Category, then they are shown in the "Other" section of the Category page. See [Site model](#site-model) for supported First Tags. |
| `image` | _Optional_. It is highly recommended to supply an image. For Posts the image is displayed above the body text, and shown on landing page and category pages. It is also used for link previews ([OpenGraph](https://ogp.me/), [Twitter Cards](https://developer.twitter.com/en/docs/twitter-for-websites/cards/guides/getting-started)). Note that depending on display location, top and bottom may be sheared off, hence decorative images work best. On Posts no image path should be specified. |
| `status` | _Optional, only applies to Posts, requires permission_. Posts can be either `featured` or `sticky`. Setting this field requires permission by a site maintainer, by placing a request in the site's [issue tracker](https://codeberg.org/fediverse/fedi-foundation/issues).
| `layout` | _Optional, only applies to Pages_. For Pages you can choose different page layouts, like `page-sidebar`. More layouts will be added as the site evolves. Posts have the default layout of `post`. |
| `permalink` | _Optional, only applies to Pages_. Use to to specify the URL path the Page should have when displayed in the browser address bar. When omitted the filename determines the URL path.

If you want to create and publish a Page with a new layout, then first consult with the site maintainers via the [issue tracker](https://codeberg.org/fediverse/fedi-foundation/issues).

#### Folder structure

The source code of this website lives at Codeberg in the [fedi-foundation](https://codeberg.org/fediverse/fedi-foundation) repository. The project has the following folder structure:

```sh
[root]
   ├── _data
   │     └── people.yml  # list of SocialHub members and authors
   ├── _includes         # building blocks for the website
   ├── _layouts          # page layouts that are available
   ├── _pages            # all the Pages that have been defined
   │     ├── about.md    # the /about page of the website
   │     └──  ...        # your custom Page here
   ├── _posts            # all the Posts that have been published
   │     └──  ...        # your published Post here
   ├── _sass             # styles in SCSS to be compiled to CSS
   ├── _site             # the compiled output of the website
   ├── assets            # folder static site resources
   │     ├── css         # static, non-compiled CSS files
   │     ├── fonts       # custom fonts to be loaded
   │     └── images      # all the site images (see below)
   ├── _config.yml       # website configuration
   ├── index.html        # website landing page
   └── README.md         # repository description text
```
{: style="line-height:1.2"}

Important for publishing are the `_pages`, `_posts` and `assets/images` folders:

- Pages and Posts must be placed in the corresponding folders.
  - They are Markdown documents having a `.md` extension.
- Post filename must begin with publishing date in this format `YYYY-MM-DD`.
  - For example this post `2021-03-30-my-post-is-here.md`.
  - Will be published at this URL path `/2021/03/my-post-is-here/`
- Images must be placed in `/assets/images/posts` folder.
  - A Post image should be around 750 x 500 pixels.
  - Images should be optimized, and ideally be smaller than 100 kilobytes.
  - Always give an alt-text: `![my-alt-text](/assets/images/posts/img-filename)`

## Licensing

The default license used on the website is [Creative Commons CC0 1.0 Universal (CC0 1.0) Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/), but for individual posts and pages this can be overridden with the `license` frontmatter setting (see [Page design](#page-design)). Supported alternative licenses are:

- [Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/)
- [Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)](https://creativecommons.org/licenses/by-nc-sa/4.0/)

The different license that is used is indicated in three different places:

- In the page body below the Author's bio if an `author` is specified.
- In the footer of the website, which will change accordingly.
- In the SEO metadata of the page that search engines use for indexing.

A public domain dedication means you waive all copyright claims to your work and donate it to the public, while the alternative licenses allow you to retain those rights.

Profile pages that are part of the [People Directory](/community.html#people) are licensed `CC BY-NC-SA` by default. This cannot be overridden.

## Adding authors

To publish your own pages or co-author with others, you must provide author details and optionally create a SocialHub member profile for inclusion in the [People Directory](/community.html#people).

#### Provide author details

**TODO**: Document this section.

#### Create member profile

**TODO**: Document this section.

## Editing content

There are two options for editing site contents:

1. Using the Codeberg user interface directly from the browser.
2. Using your local computer and submit via Git version control.

We recommend using Git as it gives more control. All editing occurs on your own fork (copy) of the site content. When done editing changes are submitted for review with a Pull Request. First fork the repository to your own Codeberg account:

![Fork the Fedi Foundation repository](/assets/images/posts/gitea-fork-repository.png)

#### Using the browser

To modify an existing page, navigate to it and click the 'Edit' icon:

![Click the Pencil icon to start editing](/assets/images/posts/gitea-start-page-editing.png)

Create a page by selecting 'New File' to edit in the user interface or 'Upload File':

![Create a new page](/assets/images/posts/gitea-create-file.png)

Now you can edit the page by typing Markdown-formatted text:

![Edit a page as Markdown text](/assets/images/posts/gitea-edit-markdown.png)

When you are done making changes, then it is time to send the Pull Request:

![Create a Pull Request](/assets/images/posts/gitea-create-pull-request.png)

After clicking 'Propose file change' the changes are sent to the source repository for review.

#### Using Git

In order to edit on your local computer you must checkout **your fork** of the [fedi-foundation](https://codeberg.org/fediverse/fedi-foundation) repository. Ensure you have [Git](https://git-scm.com/) on your computer, or otherwise install it first. Now, note that Git can take some time to get used to. It has a lot of functionality for source code revision control. Luckily we don't need most of it. Here is a [Git Cheat Sheet](https://about.gitlab.com/images/press/git-cheat-sheet.pdf) (PDF) with an overview of most-used commands. 

Copy the clone URL of your fork via the user interface:

![Clone the URL of the repository](/assets/images/posts/gitea-get-clone-url.png)

Now on the command line in the directory where you want to checkout the repository type:

```sh
git clone <my-forked-repository-url>
```

The entire repository is now copied to `/fedi-foundation` in your folder, and ready for editing using any text editor. If you already did this before, you must ensure that your local copy is up-to-date with the source repository so that you are not editing outdated files (also explained in [this Blog post](https://ardalis.com/syncing-a-fork-of-a-github-repository-with-upstream/)).

To do so _for the first time_ you must first make Git aware of the location of the source repository (retrieve the URL in the same way as you did for your fork):

```sh
git remote add upstream <fedi-foundation-source-repository-url>
```

With the remote added, you can fetch any changes and merge them in your local copy:

```sh
git fetch upstream
git merge upstream/main
```

Now your local repository is up-to-date and you can start editing.

All edits you make are tracked automatically, but new files need to be added first. You can type `git status` for an overview. To add files type:

```sh
git add <file1> <file2>  # add untracked files
git add -A               # or add all untracked files in one go
```

Once you have completed a number of modifications that represent something to publish, you create a commit. In the following example two commits are created. One represents adding a new page, and one represents adding author details and an author profile:

```sh
# Commit the new page that explains how to publish.
git commit _pages/publish.md -m "Add publishing tutorial"

# All other changes relate to author. Adding -a includes them in one go.
git commit -am "Add 'Alice' author details and profile"
```

Done editing, changes made, it is now time to send them to your fork:

```sh
git push
```

You are asked to log in using your Codeberg account, after which commits are transfered. The only remaining taks is to create the Pull Request, and this is done on your fork using the Codeberg user interface:

![Create new Pull Request from your fork](/assets/images/posts/gitea-git-new-pull-request.png)

Give the pull request a clear Title and Description and after submiting the commits are sent to the source repository for review.