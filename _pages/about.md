---
title: "About Us"
layout: page-sidebar
permalink: "/about.html"
---

Welcome to the Federated Diversity Foundation! We exist to empower the [SocialHub](https://socialhub.activitypub.rocks) community, in her mission to evolve the Fediverse technology landscape and open standards.

## What we do

![Foundation • SocialHub • Ecosystem • Fediverse](/assets/images/posts/foundation-lightbulb.png)

We offer support for technology and people. Together we evolve a vibrant ecosystem so the Fediverse can truly shine. 

- **We empower community.** Strong communities build strong ecosystems. We are dedicated to the success of the SocialHub, our community, and we support our fellow members. Together we ensure that Activitypub rocks.

- **We foster cooperation.** Creating a solid technology foundation is a collective effort. We streamline organization and processes to make that easier. Encouraging member interaction, and extending relationships across the web.

- **We facilitate healthy growth.** We want more technologists to join the fray. To make Fediverse stronger, evolve our open standards, and facilitate a vibrant ecosystem of free software projects that interoperate.

- **We advocate our work**. More people should know what we do, and why we do it. Our vision is one of "Social Networking Reimagined". We spread the message of Humane Technology and the promise of being United in Diversity.

## Who we are

Fedi foundation is maintained and operated by a team of SocialHub members. Content on this site is created collectively by our entire member base. Read [How You Can Publish On Fedi Foundation](/publish.html).

## Build the Fediverse

[The Fediverse is like Spiral Island](/2021/04/fediverse-spiral-island-analogy/): A tiny hand-made structure floating in rough seas. It holds a promise for a better future. But its continued existence is by no means secure. Only with concerted effort, and people cooperating from all across the globe, can it become a sprawling archipelago. A place of beauty and humanity.

Our [SocialHub](https://socialhub.activitypub.rocks) community, is among its builders. We invite you to help build with us too.

## Support our work

Community work is _very_ time-consuming. It includes the boring tasks, doing the chores, so that others benefit and have a great experience. It is also essential, even vital work. Without people consistently dedicated to community health, it eventually deteriorates. Progress dwindles or even stalls altogether. The casual volunteer that jumps in now and then cannot uphold a community. Without passionate people doing the - often under-appreciated - tasks, communities fail.

Your help is needed so we can pay for infrastructure, and provide small compensation for those who sacrifice their time to do this noble work. Read [How You Can Contribute](/donate.html) and for what we use your support.