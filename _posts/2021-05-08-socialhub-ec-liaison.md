---
title: "The European Commission and the Fediverse"
description: "As EU institutions make first strides on the Fediverse, we need a broad discussion on implications and to explore the opportunities."
author: aschrijver
categories: foster
tags: [advocacy, cooperation, government]
status: featured
image: pexels-pixabay-434673.jpg
---

The Fediverse is increasingly making waves in the broader tech world and beyond. Not only do we provide a decentralized alternative to Big Tech platforms, but also - with our diverse and open culture and humane technology - we pave the way to [_**"Reimagine Social Networking"**_](/2021/04/social-networking-reimagined/).

As such we have attracted the attention of the European Commission (EC) who view ActivityPub and related protocols as candidates to play a central role [shaping Europe's digital future](https://ec.europa.eu/info/strategy/priorities-2019-2024/europe-fit-digital-age/shaping-europe-digital-future_en). A future where interoperability based on open standards provides equal access and a level playing field for parties large and small, and for EU residents. Also our federated apps are perfect matches for the EC's [open source software strategy](https://ec.europa.eu/info/departments/informatics/open-source-software-strategy_en).

Therefore, when we were approached by the EC, we organized the introductary 3-part ["ActivityPub for Administrations"](https://socialhub.activitypub.rocks/t/ec-ngi0-liaison-webinars-and-workshop-april-2021/1539) event. The event was a great success, and EC participants are very eager to learn more.

## A Call to Action

Given these recent and important developments we at [SocialHub](https://socialhub.activitypub.rocks) want to start a broad discussion on all the ins and outs, implications and opportunities of EU institutions and the role of EU government administrations in relation to the Fediverse.

You as fellow fedizens ideally should all become actively involved. To voice your opinion, provide your feedback and to enlist your help and collaboration so we move in the right direction. One that benefits us all.

We know many fedizens are quite sceptical about any form of government involvement, either directly or indirect. There's mistrust and worry. Fear about control, surveillance and restrictions of freedom. On the other hand there's also prejudice and quick judgment to overcome.

Whatever your perspective and views, all of them are valid. We need broad and frank discussion, wariness and critical-thinking. But also willingness to look at the Fediverse future with an open mind and reason.

## Threats and opportunities

On the Fediverse we do not operate in a vacuum. Whatever we do, we are bound by the jurisdiction of our country and need to adhere to its rules and regulations. Arguments such as _"EU is evil"_ and therefore shunning it are not only overly simplistic, but effectively rule out any opportunity to influence government policy and help bring improvements.

Governments are not single anonymous entities, there are many different forces at play. Some beneficial and promising, and others terrifying and dystopic (e.g. [TERREG](https://www.patrick-breyer.de/en/controversial-eu-anti-terror-internet-regulation-terreg-about-to-be-adopted/) comes to mind). With more interaction and stronger bonds, fedizens can reinforce the former and make a stronger fist against the latter.

Greater awareness at the EU that the Fediverse exists and offers much better and more humane alternatives to Big Tech may lead to many opportunities. Benefits in the form of increased funding and other forms of help, but also regulations that makes our position stronger.

There are threats too, and there may be many of them. One of them is the danger that with funding comes dependence and possibly subservience and co-option. We see this for instance with non-profits and conferences accepting Big Tech sponsorship. On the other hand EU institutions NOT being well-acquainted with what the Fediverse offers, constitutes a threat in itself. They may perceive it as some Dark Web where extremism and illegality festers, and must be controlled.

## Join our Policy Group

At the [SocialCG Meetup on 15 January](https://www.w3.org/2021/01/15-social-minutes.html) we decided to initiate the **Policy Special Interest Group**:

> The remit of the Policy Group is primarily to liaise with existing digital/tech political lobbying groups to bring awareness of relevant standards and W3C recommendations which can then be relayed at the right times and in the right places to politicians and policy makers. As part of this, the Policy Group may organise events, briefings, draft documentation, proposals, or open letters which can be co-signed by the SocialCG.

In addition to that the Policy group:

* Can use its best judgement to decide which relevant groups to reach out to in its efforts
* Establishes a regular schedule to update the wider SocialCG with anything new.
* Will coordinate activities publicly on the SocialHub forum.

SocialHub members are eligible for group membership and a dedicated [Fediverse Policy SIG](https://socialhub.activitypub.rocks/c/meeting/fediverse-policy/59) forum section is available to the group.

If you want to play an active role in shaping our liaison to the European Commission and EU institutions we encourage you to join the Policy SIG. 

<!-- Photo by Pixabay from Pexels
     https://www.pexels.com/photo/architecture-bridge-building-business-434673/
-->