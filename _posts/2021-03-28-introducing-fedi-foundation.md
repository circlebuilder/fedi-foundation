---
title: "Introducing the Federated Diversity Foundation"
description: "Foundation for community empowerment, encouraging healthy evolution of Fediverse technology."
author: aschrijver
categories: community
tags: [vision, roadmap]
status: sticky
image: pexels-anna-shvets-4312861.jpg
---

Welcome to the brand new website of the Federated Diversity Foundation, or simply _fedi foundation_. This article provides more background on why the foundation was created as a companion to the [SocialHub](https://socialhub.activitypub.rocks) community forum, and for what purpose it exists.

#### Contents

- [History of the SocialHub](#history-of-the-socialhub)
- [Barriers to fediverse evolution](#barriers-to-fediverse-evolution)
- [How fedi foundation empowers socialhub](#fedi-foundation-empowers-socialhub)
  - [Community empowerment](#community-empowerment)
  - [Facilitating cooperation](#facilitating-cooperation)
  - [Stimulating growth](#stimulating-growth)
  - [Boosting advocacy](#boosting-advocacy)
- [We foster community and our member base](#we-foster-community-and-our-member-base)
  - [Why you should support us financially](#why-you-should-support-us-financially)

---

## History of the SocialHub

The [SocialHub community](https://socialhub.activitypub.rocks) was founded in September 2019, and has played a central role in the development of Fediverse-related technologies ever since. Though SocialHub is but small, some of our members were instrumental to the success of the Fediverse we have today. Active participants are from the [W3C Social Web Incubator Community Group](https://www.w3.org/community/SocialCG/), the W3C [ActivityStreams](https://www.w3.org/TR/activitystreams-core/) and [ActivityPub](https://www.w3.org/TR/activitypub/) recommendation authors, and the early adopters of the technology.

The Fediverse was created in true grassroots fashion. A delightful social media fabric that is home to over four million fedizens. Together we evolved the technology landscape, plugging holes that still existed in the W3C standards, and building custom extensions on top of them. Many production-ready projects have proven their worth in real-world applications.

## Barriers to Fediverse evolution

Currently we are at an important stage of our development. The tech world around us is changing rapidly, and not in a good direction. There are many threats for our future, our continued existence. Fediverse is still weak and brittle. At the same time there exist tremendous opportunities to do things better than what the corporate world has in store for us. Fediverse has vast potential. Our vision is one of [**_"Social Networking Reimagined"_**](/2021/04/social-media-reimagined/).

However, some inhibiting factors are holding us back. An overly technical and inward focus, lack of time to coordinate. Plus our grassroots nature, even decentralization itself, leads to fragmentation and ad-hoc development. They all slow down our evolution as a collective.

> We lack common focus and direction, clear strategy and processes to attain our vision, and committed people to organize all this.

This is understandable. Software developers just want to code, dedicated to their own projects. Spending time on other things is seen as a distraction. But eventually this means these projects will hurt themselves.

## Fedi Foundation empowers SocialHub

More is needed to go next-level. We must evolve new versions of our technology standards, and design the capabilities to build code that interoperates at a deeper level. Open issues in the community must be addressed and we need more members to help with that. We must coordinate more, and collaborate with different groups.

This requires a lot of community effort. We need people dedicated to leading the SocialHub forwards, who help with organization, community engagement and membership growth, and doing many of the chores that need to be done. The importance of this time-consuming work is much underappreciated.

> Health and future of any federated project depends directly on the health of the SocialHub. Active participation yields a win-win for all fedi developers.

Fedi Foundation supports SocialHub members that are actively engaged. To the SocialHub community as a whole she serves as facilitator and provides the following services:

- Community empowerment
- Facilitating cooperation
- Stimulating growth
- Steering advocacy

The Fedi Foundation is organized as a dedicated team of SocialHub members. Read more in-depth information about our mission in [Introducing the Federated Diversity Foundation](/2021/03/introducing-fedi-foundation/).

#### Community empowerment

Empowerment follows from knowing the exact state of the technology landscape and our plans for the future. Furthermore any SocialHub member must have a clear and strong voice and ability to help steer the evolution of the technology base. Also Fedi Foundation will actively seek funding opportunities for its members, to support community activities.

#### Facilitating cooperation

Streamlining the community organization and processes is an important task. Who does what, and who should you contact? We track open issues in the community, and bring them to attention to the right people at the right time. We help organize events, conferences and hackathons, and provide the community with online tools to do her work.

#### Stimulating growth

The SocialHub community is but very small. The majority of federated apps are not under its umbrella, and the majority of its members are programmers. Other IT skills are underrepresented. So are minority groups. For the community to be healthy both membership and member diversity needs to increase. Fedi Foundation will seek to maximize the incentives to join our group.

#### Boosting advocacy

The world at large deserves to know about the Fediverse. There are a huge amount of people very unhappy with the traditional social media we have today. And following from that technologists and policy makers should know about better ways of doing things. Of building groundbreaking applications, exploring new capabilities, and doing things that are morally and ethically sound. At Fedi Foundation we'll advocate our exciting story to the world, and we entice others to campaign with us.

## We foster community and our member base

Powerful communities build powerful ecosystems. Vibrant community health and strength is vital for the future of the Fediverse. Yet no one wants to do the chores, and prolonged dedication to community tasks is in general an unthankful job. App developers passionately on the other hand want _and need_ to dedicate fully to project success. Free software projects face the same issue, and need to be supported just as much. One thing is clear in this..

> Time is the most precious commodity in the world today.

To mitigate this the Fedi Foundation will actively seek to develop all kinds of incentives to stimulate anyone to participate actively in the SocialHub community, in pursuit of healthy growth and evolution of the Fediverse. And we want you to be part of our success formula.

Our incentives will include financial support. We will seek both donations and grants. First and foremost the beneficiaries will be those who help foster the community on a near daily basis. Secondary beneficiaries are those SocialHub members performing the occasional designated community tasks.

#### Why you should support us financially

You are delighted by what you find on the Fediverse, and passionate about its values and principles, the culture you encounter. You want to see it blossom ever further and enjoy the applications and services that are part of it. The tech world around you worries you, with its corporatism and surveillance capitalism. You want a radically new, fresh and exciting social fabric to arise, where you can flourish online. This space you want to be as humane, healthy and protected as possible. Made for the people and create by the people. You want to feel safe and secure. You want to be among friends.

> Help SocialHub thrive, so we can cocreate our fedi future.

If any of these things appeal to you, then you must consider if it is worth some money that you can spare. Because we are fragile still, and none of our visions and dreams are yet assured to become reality as stable pillars of society.

Help SocialHub thrive, so we can cocreate our fedi future. Please [**Support Us**](/support.html) and consider to [**Sign Up**](https://socialhub.activitypub.rocks) as SocialHub member. 

<!-- Photo by Anna Shvets from Pexels
     https://www.pexels.com/photo/wood-industry-tools-knife-4312861/
 -->