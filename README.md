<div align="center">

[![Fediverse Foundation](assets/images/fedi-foundation-logo-black.png)](https://fedi.foundation)

### [SocialHub community](https://socialhub.activitypub.rocks) | [IRC #social channel](https://irc.w3.org/?channels=social) | [Github Pages repo](https://github.com/fullcircle-today/groundwork)

</div>

Welcome to the Fedi Foundation website source repository. 

## Contributing

We'd love your participation to make this site better and more informative. Read here:

- [How to contribute to the site's source code](CONTRIBUTING.md)
- [How to publish pages to the Fedi Foundation](https://fedi.foundation/publish/)

If you find a bug or have suggestions and ideas for improvement we encourage you to [create an issue](https://codeberg.org/fediverse/fedi-foundation/issues) in our tracker.

## Credits

Many thanks to [Codeberg](https://codeberg.org/) for actively supporting Free and Open Software Development and hosting our code. Detailed [Credits](https://fedi.foundation/credits/) are published on the website.

## License

The website source code is available und an [AGPL-3.0](LICENSE) license.

Any content on the website is available to the Public Domain [CC Zero](https://creativecommons.org/publicdomain/zero/1.0/), unless a page is specifically published using a different license. In these case this is mentioned in the body text, as well as in the page footer.

Supported alternative page licenses are [CC BY-SA-4.0](https://creativecommons.org/licenses/by-sa/4.0/) and [CC BY-NC-SA-4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/). More [licensing details](https://fedi.foundation/publish/#licensing) can be found on the website.